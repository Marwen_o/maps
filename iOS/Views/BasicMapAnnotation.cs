﻿using System;
using CoreLocation;
using MapKit;

namespace MapLikwid121016.iOS
{
	public class BasicMapAnnotation : MKAnnotation
	{
		private string _title;
		private CLLocationCoordinate2D _coordinate;
		public BasicMapAnnotation(CLLocationCoordinate2D coordinate, string title)
		{
			_coordinate = coordinate;
			_title = title;
		}
		public override CLLocationCoordinate2D Coordinate
		{
			get
			{
				return _coordinate;
			}
		}

		public override string Title
		{
			get
			{
				return _title;
			}
		}

	}
}
