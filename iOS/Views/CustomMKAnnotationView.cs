﻿using System;
using CoreLocation;
using MapKit;

namespace MapLikwid121016.iOS
{
	public class CustomMKAnnotationView  : MKAnnotationView
	{
        public string Id { get; set; }

		public string Url { get; set; }

		//private string _title;


		public CustomMKAnnotationView(IMKAnnotation annotation,string id)
		 : base(annotation, id)
		{
		}
	}
}
