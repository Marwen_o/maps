﻿using System;
using System.Collections.Generic;
using CoreGraphics;
using CoreLocation;
using MapKit;
using MapLikwid121016;
using MapLikwid121016.iOS;
using SharpMapKitClusterer;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Maps.iOS;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomMap), typeof(CustomMapRenderer))]
namespace MapLikwid121016.iOS
{
	public class CustomMapRenderer : MapRenderer
	{
		UIView customPinView;
		List<CustomPin> customPins;

		#region
		private const string kClusterAnnotationId = "REUSABLE_CLUSTER_ANNOTATION_ID";
		private const string kPinAnnotationId = "REUSABLE_PIN_ANNOTATION_ID";
		private const int kTagClusterLabel = 1;
		#endregion

		public CustomMapRenderer()
		{
		}

		protected override void OnElementChanged(ElementChangedEventArgs<View> e)
		{
			base.OnElementChanged(e);

			if (e.OldElement != null)
			{
				var nativeMap = Control as MKMapView;
				nativeMap.GetViewForAnnotation = null;
				nativeMap.CalloutAccessoryControlTapped -= OnCalloutAccessoryControlTapped;
				nativeMap.DidSelectAnnotationView -= OnDidSelectAnnotationView;
				nativeMap.DidDeselectAnnotationView -= OnDidDeselectAnnotationView;
			}

			if (e.NewElement != null)
			{
				var formsMap = (CustomMap)e.NewElement;
				var nativeMap = Control as MKMapView;
				customPins = formsMap.CustomPins;

				nativeMap.GetViewForAnnotation = GetViewForAnnotation;
				nativeMap.CalloutAccessoryControlTapped += OnCalloutAccessoryControlTapped;
				nativeMap.DidSelectAnnotationView += OnDidSelectAnnotationView;
				nativeMap.DidDeselectAnnotationView += OnDidDeselectAnnotationView;



				#region
				List<IMKAnnotation> sampleAnnotations = new List<IMKAnnotation>();
				foreach (var item in customPins)
				{
					//formsMap.Pins.Add(item.Pin);

					var annotation = new BasicMapAnnotation(new CLLocationCoordinate2D(item.Pin.Position.Latitude, item.Pin.Position.Longitude)
					                                        , "Marker "+item.Id);
					sampleAnnotations.Add(annotation);
				}

				FBClusteringManager clusteringManager = new FBClusteringManager(sampleAnnotations);
				 //Setup map region to display initially
				CLLocationCoordinate2D center = new CLLocationCoordinate2D(40.741895,-73.989308);
				MKCoordinateRegion region = MKCoordinateRegion.FromDistance(center, 200000, 200000);
				nativeMap.SetRegion(region, false);

				//// As the map moves, use the clusting manager update the clusters displayed on the map
			
				   nativeMap.RegionChanged += (object sender, MKMapViewChangeEventArgs e1) =>
					{
						double scale = nativeMap.Bounds.Size.Width / nativeMap.VisibleMapRect.Size.Width;
						List<IMKAnnotation> annotationsToDisplay = clusteringManager.ClusteredAnnotationsWithinMapRect(nativeMap.VisibleMapRect, scale);
						clusteringManager.DisplayAnnotations(annotationsToDisplay, nativeMap);
					};
				#endregion
			}
		}

		MKAnnotationView GetViewForAnnotation(MKMapView mapView, IMKAnnotation annotation)
		{
			#region
			//MKAnnotationView annotationView = null;

			//if (annotation is MKUserLocation)
			//	return null;

			//var anno = annotation as MKPointAnnotation;
			//var customPin = GetCustomPin(anno);
			//if (customPin == null)
			//{
			//	throw new Exception("Custom pin not found");
			//}

			//annotationView = mapView.DequeueReusableAnnotation(customPin.Id);
			//if (annotationView == null)
			//{
			//	annotationView = new CustomMKAnnotationView(annotation, customPin.Id);
			//	annotationView.Image = UIImage.FromFile("Drops.png");
			//	annotationView.CalloutOffset = new CGPoint(0, 0);
			//	annotationView.LeftCalloutAccessoryView = new UIImageView(UIImage.FromFile("Drops.png"));
			//	annotationView.RightCalloutAccessoryView = UIButton.FromType(UIButtonType.DetailDisclosure);
			//	((CustomMKAnnotationView)annotationView).Id = customPin.Id;
			//	((CustomMKAnnotationView)annotationView).Url = customPin.Url;
			//}
			//annotationView.CanShowCallout = true;

			//return annotationView;
			#endregion

			#region
			//MKAnnotationView anView;
			//MKPinAnnotationView tnView;

			//#region
			////cuatom pin
			////MKAnnotationView annotationView ;
			//#endregion

			//if (annotation is FBAnnotationCluster)
			//{
			//	FBAnnotationCluster annotationcluster = (FBAnnotationCluster)annotation;
			//	anView = (MKAnnotationView)mapView.DequeueReusableAnnotation(kClusterAnnotationId);
			//	tnView = (MKPinAnnotationView)mapView.DequeueReusableAnnotation(kClusterAnnotationId);
			//	#region
			////	annotationView = mapView.DequeueReusableAnnotation(kClusterAnnotationId);
			//	#endregion
			//	UILabel label = null;
			//	if (tnView == null ) //&& annotationView==null
			//	{
			//		// nicely format the cluster icon and display the number of items in it
			//		anView = new MKAnnotationView(annotation, kClusterAnnotationId);
			//		tnView = new MKPinAnnotationView(annotation,kClusterAnnotationId);
			//		#region
			//		////cuatom pin
			//		//    annotationView = new CustomMKAnnotationView(annotation, kClusterAnnotationId);
			//		//	annotationView.Image = UIImage.FromFile("Drops.png");
			//		//	annotationView.CalloutOffset = new CGPoint(0, 0);
			//		//	annotationView.LeftCalloutAccessoryView = new UIImageView(UIImage.FromFile("Drops.png"));
			//		//	annotationView.RightCalloutAccessoryView = UIButton.FromType(UIButtonType.DetailDisclosure);
			//		//((CustomMKAnnotationView)annotationView).Id = ((FBAnnotationCluster)annotation).Title;
			//		//	((CustomMKAnnotationView)annotationView).Url = ((FBAnnotationCluster)annotation).Title;
			//		#endregion
			//		anView.
			//		anView.Image = UIImage.FromBundle("cluster");
			//	//	pinview.Image=UIImage.FromBundle("Drops");
			//		label = new UILabel(new CGRect(0, 0, anView.Image.Size.Width, anView.Image.Size.Height));
			//		label.Tag = kTagClusterLabel;
			//		label.TextAlignment = UITextAlignment.Center;
			//		label.TextColor = UIColor.White;
			//		anView.AddSubview(label);
			//	//	anView.Add(pinview);
			//		anView.CanShowCallout = false;

			//	}
			//	else
			//	{
			//		label = (UILabel)anView.ViewWithTag(kTagClusterLabel);
			//	}
			//	label.Text = annotationcluster.Annotations.Count.ToString();
			//	//annotationView.CanShowCallout = true;
			//	return anView;
			//}
			//return null;
			#endregion

			#region
		//	MKAnnotationView annotationView = null;

		//	if (annotation is MKUserLocation)
		//		return null;
		////	var myannotation = new CustomMKAnnotationView(annotation, kClusterAnnotationId);
		//	var anno = annotation as MKPointAnnotation;
		//	var customPin = GetCustomPin(anno);
		//	if (customPin == null)
		//	{
		//		throw new Exception("Custom pin not found");
		//	}

		//	annotationView = mapView.DequeueReusableAnnotation(customPin.Id);
		//	if (annotationView == null)
		//	{
		//		annotationView = new CustomMKAnnotationView(annotation, customPin.Id);
		//		annotationView.Image = UIImage.FromFile("Drops.png");
		//		annotationView.CalloutOffset = new CGPoint(0, 0);
		//		annotationView.LeftCalloutAccessoryView = new UIImageView(UIImage.FromFile("Drops.png"));
		//		annotationView.RightCalloutAccessoryView = UIButton.FromType(UIButtonType.DetailDisclosure);
		//		((CustomMKAnnotationView)annotationView).Id = customPin.Id;
		//		((CustomMKAnnotationView)annotationView).Url = customPin.Url;
		//	}
		//	annotationView.CanShowCallout = true;

		//	return annotationView;
			#endregion
			#region
			MKAnnotationView anView;

					if (annotation is FBAnnotationCluster)
					{
				      // var customPin = GetCustomPin((MapKit.MKPointAnnotation)annotation);
						FBAnnotationCluster annotationcluster = (FBAnnotationCluster)annotation;
			        	anView = (MKAnnotationView)mapView.DequeueReusableAnnotation(kClusterAnnotationId);

						UILabel label = null;
						if (anView == null)
						{
							// nicely format the cluster icon and display the number of items in it
							anView = new MKAnnotationView(annotation, kClusterAnnotationId);
							anView.Image = UIImage.FromBundle("cluster");
				         //	anView.Image = UIImage.FromFile("Drops.png");
					       // anView.BackgroundColor = UIColor.Green;
							label = new UILabel(new CGRect(0, 0, anView.Image.Size.Width, anView.Image.Size.Height));
							label.Tag = kTagClusterLabel;
							label.TextAlignment = UITextAlignment.Center;
							label.TextColor = UIColor.White;
							anView.AddSubview(label);
							anView.CanShowCallout = false;
						}
						else
						{
							label = (UILabel)anView.ViewWithTag(kTagClusterLabel);
						}

						label.Text = annotationcluster.Annotations.Count.ToString();

						return anView;
					}
					return null;
			#endregion
			//m5alta
			#region 
			//MKAnnotationView anView;
			//var anno = annotation as MKPointAnnotation;
			//var customPin = GetCustomPin(anno);
			//if (customPin == null)
			//{
            //  throw new Exception("Custom pin not found");
			//}
			//if (annotation is FBAnnotationCluster)
			//{
			//	FBAnnotationCluster annotationcluster = (FBAnnotationCluster)annotation;
			//	anView = (MKAnnotationView)mapView.DequeueReusableAnnotation(kClusterAnnotationId);

			//	UILabel label = null;
			//	if (anView == null)
			//	{
			//		// nicely format the cluster icon and display the number of items in it
			//		anView = new MKAnnotationView(annotation, kClusterAnnotationId);
			//		anView.Image = UIImage.FromBundle("cluster");
			//		label = new UILabel(new CGRect(0, 0, anView.Image.Size.Width, anView.Image.Size.Height));
			//		label.Tag = kTagClusterLabel;
			//		label.TextAlignment = UITextAlignment.Center;
			//		label.TextColor = UIColor.White;
			//		anView.AddSubview(label);
			//		anView.CanShowCallout = false;
			//	}
			//	else
			//	{
			//		label = (UILabel)anView.ViewWithTag(kTagClusterLabel);
			//	}

			//	label.Text = annotationcluster.Annotations.Count.ToString();

			//	return anView;
			//}

			//return null;
			#endregion
		}

		void OnCalloutAccessoryControlTapped(object sender, MKMapViewAccessoryTappedEventArgs e)
		{
			var customView = e.View as CustomMKAnnotationView;
			if (!string.IsNullOrWhiteSpace(customView.Url))
			{
				UIApplication.SharedApplication.OpenUrl(new Foundation.NSUrl(customView.Url));
			}
		}

		void OnDidSelectAnnotationView(object sender, MKAnnotationViewEventArgs e)
		{
			var customView = e.View as CustomMKAnnotationView;
			customPinView = new UIView();

			if (customView.Id == "Xamarin")
			{
				customPinView.Frame = new CGRect(0, 0, 200, 84);
				var image = new UIImageView(new CGRect(0, 0, 200, 84));
				image.Image = UIImage.FromFile("Drops.png");
				customPinView.AddSubview(image);
				customPinView.Center = new CGPoint(0, -(e.View.Frame.Height + 75));
				e.View.AddSubview(customPinView);
			}
		}

		void OnDidDeselectAnnotationView(object sender, MKAnnotationViewEventArgs e)
		{
			if (!e.View.Selected)
			{
				customPinView.RemoveFromSuperview();
				customPinView.Dispose();
				customPinView = null;
			}
		}

		CustomPin GetCustomPin(MKPointAnnotation annotation)
		{
			var position = new Position(annotation.Coordinate.Latitude, annotation.Coordinate.Longitude);
			foreach (var pin in customPins)
			{
				if (pin.Pin.Position == position)
				{
					return pin;
				}
			}
			return null;
		}

		public void clusteritems()
		{ 
			 // Create a random set of map locations
   //         List<IMKAnnotation> sampleAnnotations = new List<IMKAnnotation>();
			//Random rnd = new Random();

			//for (int i = 1; i <= 500; i++)
			//{
			//	double lat = 51.7 + rnd.NextDouble();
			//	double lon = 4.3 + rnd.NextDouble();
			//	var annotation = new BasicMapAnnotation(new CLLocationCoordinate2D(lat, lon), "Marker " + i);
			//	sampleAnnotations.Add(annotation);
			//}

			//// Setup clustering manager
			//FBClusteringManager clusteringManager = new FBClusteringManager(sampleAnnotations);

			// Setup map region to display initially
			//CLLocationCoordinate2D center = new CLLocationCoordinate2D(52.2, 4.8);
			//MKCoordinateRegion region = MKCoordinateRegion.FromDistance(center, 200000, 200000);
			//MapView.SetRegion(region, false);

			//// As the map moves, use the clusting manager update the clusters displayed on the map
			//MapView.RegionChanged += (object sender, MKMapViewChangeEventArgs e) =>
			//	{
			//		double scale = MapView.Bounds.Size.Width / MapView.VisibleMapRect.Size.Width;
			//		List<IMKAnnotation> annotationsToDisplay = clusteringManager.ClusteredAnnotationsWithinMapRect(MapView.VisibleMapRect, scale);
			//		clusteringManager.DisplayAnnotations(annotationsToDisplay, MapView);
			//	};

			// Display a custom icon for the clusters
			//MapView.GetViewForAnnotation += (mapView, annotation) =>
			//	{
			//		MKAnnotationView anView;

			//		if (annotation is FBAnnotationCluster)
			//		{
			//			FBAnnotationCluster annotationcluster = (FBAnnotationCluster)annotation;
			//			anView = (MKAnnotationView)mapView.DequeueReusableAnnotation(kClusterAnnotationId);

			//			UILabel label = null;
			//			if (anView == null)
			//			{
			//				// nicely format the cluster icon and display the number of items in it
			//				anView = new MKAnnotationView(annotation, kClusterAnnotationId);
			//				anView.Image = UIImage.FromBundle("cluster");
			//				label = new UILabel(new CGRect(0, 0, anView.Image.Size.Width, anView.Image.Size.Height));
			//				label.Tag = kTagClusterLabel;
			//				label.TextAlignment = UITextAlignment.Center;
			//				label.TextColor = UIColor.White;
			//				anView.AddSubview(label);
			//				anView.CanShowCallout = false;
			//			}
			//			else
			//			{
			//				label = (UILabel)anView.ViewWithTag(kTagClusterLabel);
			//			}

			//			label.Text = annotationcluster.Annotations.Count.ToString();

			//			return anView;
			//		}

			//		return null;
			//	};

		}
	}
}
