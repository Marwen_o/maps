﻿using System;
using Xamarin.Forms.Maps;

namespace MapLikwid121016
{
	public class CustomPin
	{
		public CustomPin()
		{
		}
		public Pin Pin { get; set; }
		public string Id { get; set; }
		public string Url { get; set; }
		public CustomPin(Pin Pin, String Id, String Url)
		{
			this.Pin = Pin;
			this.Id = Id;
			this.Url = Url;
		}
	}
}
