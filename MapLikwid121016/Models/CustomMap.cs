﻿using System;
using System.Collections.Generic;

namespace MapLikwid121016
{
	public class CustomMap : Xamarin.Forms.Maps.Map
	{
		public CustomMap()
		{
		}
		public List<CustomPin> CustomPins { get; set; }

		private Parameter _parameter;
		public Parameter Parameter
		{
			get { return _parameter; }
			set
			{
				_parameter = value;
				OnPropertyChanged();
			}
		}
	}
	public enum Parameter
	{
		test,
	}
}
