﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Android.App;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;
using Android.Graphics.Drawables;
using Android.Locations;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Com.Google.Maps.Android.Clustering;
using MapLikwid121016;
using MapLikwid121016.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Maps.Android;

[assembly: ExportRenderer(typeof(CustomMap), typeof(CustomMapRenderer))]
namespace MapLikwid121016.Droid
{
	public class CustomMapRenderer : MapRenderer,
		ILocationListener, GoogleMap.IInfoWindowAdapter,
			  IOnMapReadyCallback,
		  ClusterManager.IOnClusterClickListener,
		ClusterManager.IOnClusterItemClickListener,
		  ClusterManager.IOnClusterInfoWindowClickListener,
		ClusterManager.IOnClusterItemInfoWindowClickListener
	{
		#region
		const string TAG = "GoogleMapClustering";
		private CustomMap CMap { get; set; }
		GoogleMap map;
		List<CustomPin> customPins { get; set; }
		// List<CustomClusterPin> customClusterPins { get; set; }
		bool isDrawn;
		Location _currentLocation;
		ClusterManager _clusterManager;
		Com.Google.Maps.Android.MarkerManager _MarkerManager;
		Com.Google.Maps.Android.MarkerManager.Collection _MarkerManagerCollection;
		//DefaultRender _Defaultrender;
		Com.Google.Maps.Android.Clustering.View.DefaultClusterRenderer _DefaultClusterRenderer;
		LocationManager _locationManager;
		string _locationProvider;
		#endregion
		public CustomMapRenderer() : base()
		{
			CMap = new CustomMap();
			InitializeLocationManager();
		}
		protected override void OnElementChanged(Xamarin.Forms.Platform.Android.ElementChangedEventArgs<Xamarin.Forms.View> e)
		{
			base.OnElementChanged(e);
			if (e.OldElement != null)
			{
				map.InfoWindowClick -= OnInfoWindowClick;
			}
			if (e.NewElement != null)
			{
				var formsMap = (CustomMap)e.NewElement;
				customPins = formsMap.CustomPins;
				((Android.Gms.Maps.MapView)Control).GetMapAsync(this);
			}
		}
		public void OnMapReady(GoogleMap googleMap)
		{
			map = googleMap;
			map.InfoWindowClick += OnInfoWindowClick;
			_MarkerManager = new Com.Google.Maps.Android.MarkerManager(map);
			_MarkerManagerCollection = new Com.Google.Maps.Android.MarkerManager.Collection(_MarkerManager);
			_clusterManager = new ClusterManager(Context, map, _MarkerManager);
			SetupMapClustingDemo();
		}
		public void SetupMapClustingDemo()
		{
			if (map == null || _currentLocation == null) // Do we have a map and location avaialble?
				return;
			_clusterManager = new ClusterManager(Android.App.Application.Context, map);
			_clusterManager.SetOnClusterClickListener(this);
			_clusterManager.SetOnClusterItemClickListener(this);
			//_clusterManager.SetOnClusterInfoWindowClickListener(this);
			//_clusterManager.SetOnClusterItemInfoWindowClickListener(this);
			map.SetOnCameraChangeListener(_clusterManager);
			map.SetOnMarkerClickListener(_clusterManager);
			_currentLocation.Latitude = 36.83543436404748;
			_currentLocation.Longitude = 10.238987484454494;
			SetViewPoint(map, new LatLng(_currentLocation.Latitude, _currentLocation.Longitude), true);
			AddClusterItems(_currentLocation);
		}
		private void AddClusterItems(Location currentLocation)
		{
			//change clusterItem to CustomClusterPin
			var test = customPins;
			var testcluster = new List<ClusterItem>();
			var items = new List<ClusterItem>();
			//var currentMarker = new MarkerOptions();
			//var me = new LatLng(currentLocation.Latitude, currentLocation.Longitude);
			//currentMarker.SetPosition(me);
			//currentMarker.SetIcon(BitmapDescriptorFactory.FromResource(Resource.Drawable.icon));
			//var meMarker = new CircleOptions();
			//meMarker.InvokeCenter(me);
			//meMarker.InvokeRadius(32);
			//meMarker.InvokeStrokeWidth(0);
			//meMarker.InvokeFillColor(Android.Support.V4.Content.ContextCompat.GetColor(Context, Android.Resource.Color.HoloBlueLight));
			//map.AddCircle(meMarker);
			for (int i = 0; i < 10; ++i)
			{
				var t = i * System.Math.PI * 0.33f;
				var r = 0.005 * System.Math.Exp(0.1 * t);
				var x = r * System.Math.Cos(t);
				var y = r * System.Math.Sin(t);
				items.Add(new ClusterItem(currentLocation.Latitude + x, currentLocation.Longitude + y));
			}

			foreach (var item in test)
			{
				testcluster.Add(new ClusterItem
				{
					Position = new LatLng(item.Pin.Position.Latitude, item.Pin.Position.Longitude),
					CustomPin = item
				});
			}
			var defaultme = new DefaultClusterMapRenderer(Context, map, _clusterManager);
			_clusterManager.SetRenderer(defaultme);
			_clusterManager.AddItems(items);
	       _clusterManager.AddItems(testcluster);
		}
		public void OnLocationChanged(Location location)
		{
			Log.Debug(TAG, "OnLocationChanged");
			_currentLocation = location;
			if (_currentLocation == null)
			{
				//Toast.MakeText(BaseContext, "Unable to determine your location, using Seattle", ToastLength.Long).Show();
				_currentLocation = new Location(string.Empty)
				{
					Latitude = 36.83543436404748,
					Longitude = 10.238987484454494
				};
			}
			_locationManager.RemoveUpdates(this); // just a one-shot location update for this demo
			SetupMapClustingDemo();
		}
		void InitializeLocationManager()
		{
			if (Context != null)
			{
				_locationManager = (LocationManager)Context.GetSystemService(Android.Content.Context.LocationService);
			}
			var criteriaForLocationService = new Criteria
			{
				Accuracy = Accuracy.Fine
			};
			IList<string> acceptableLocationProviders = _locationManager.GetProviders(criteriaForLocationService, true);

			if (acceptableLocationProviders.Any())
			{
				_locationProvider = acceptableLocationProviders.First();
			}
			else
			{
				_locationProvider = string.Empty;
			}
			Log.Debug(TAG, "Using " + _locationProvider + ".");
			_locationManager?.RequestLocationUpdates(_locationProvider, 0, 0, this);
		}
		protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);
			//  _clusterManager = new ClusterManager(Android.App.Application.Context, map);
			if (e.PropertyName.Equals("VisibleRegion") && !isDrawn)
			{
				map.Clear();
				//  CMap = new CustomMap();

				//   CMap.CustomPins = new List<CustomPin>();
				////   customClusterPins = new List<CustomClusterPin>();
				//   for (int i = 0; i < 5; ++i)
				//   {
				//       var t = i * System.Math.PI * 0.33f;
				//       var r = 0.005 * System.Math.Exp(0.1 * t);
				//       var x = r * System.Math.Cos(t);
				//       var y = r * System.Math.Sin(t);
				//       //change customPin to CustomClusterPin
				//       //test1
				//       CustomPin pin2 = new CustomPin(
				//                       new Pin
				//                       {
				//                           //  Icon= Xamarin.Forms.GoogleMaps.BitmapDescriptorFactory.(Resource.Drawable.Drops),
				//                           Type = PinType.Place,
				//                           Position = new Position(47.59978 + x, -122.3346 + y),
				//                           Label = "Party At" + x,
				//                           Address = "Fox Theater" + y,
				//                       },
				//                   "cogite",
				//                   "https://developer.xamarin.com/recipes/android/fundamentals/activity/start_activity_for_result/"
				//                   );
				//       //customClusterPins.Add(pin2);
				//       CMap.CustomPins.Add(pin2);
				//   }

				//   //    int i = 0;
				//   //CMap.CustomPins change to customClusterPins
				//   foreach (var pin in customPins)
				//   {
				//       var marker = new MarkerOptions();
				//       marker.SetPosition(new LatLng(pin.Pin.Position.Latitude, pin.Pin.Position.Longitude));
				//       marker.SetTitle(pin.Pin.Label);
				//       marker.SetSnippet(pin.Pin.Address);
				//       marker.SetIcon(BitmapDescriptorFactory.FromResource(Resource.Drawable.Drops));
				//       //map.AddMarker(marker);
				//      //** _clusterManager.AddItem(new customCluster(pin.Pin.Position.Latitude, pin.Pin.Position.Longitude));
				//       _clusterManager.Cluster();
				//   }
				//   isDrawn = true;
				//   var defaultme = new DefaultClusterRender(Context, map, _clusterManager);
				//   _clusterManager.SetRenderer(defaultme);

			}
		}
		public void SetViewPoint(GoogleMap googleMap, LatLng latlng, bool animated)
		{
			CameraPosition.Builder builder = CameraPosition.InvokeBuilder();
			builder.Target(latlng);
			builder.Zoom(12f);
			CameraPosition cameraPosition = builder.Build();

			if (animated)
				googleMap.AnimateCamera(CameraUpdateFactory.NewCameraPosition(cameraPosition));
			else
				googleMap.MoveCamera(CameraUpdateFactory.NewCameraPosition(cameraPosition));
		}
		protected override void OnLayout(bool changed, int l, int t, int r, int b)
		{
			base.OnLayout(changed, l, t, r, b);

			if (changed)
			{
				isDrawn = false;
			}
		}
		public void OnInfoWindowClick(object sender, GoogleMap.InfoWindowClickEventArgs e)
		{
			//   e.Marker.ShowInfoWindow();
			var customPin = e.Marker;
			if (customPin == null)
			{
				throw new Java.Lang.Exception("Custom pin not found");
			}
			if (!string.IsNullOrWhiteSpace(customPin.Id))
			{
				//var url = Android.Net.Uri.Parse(customPin.Url);
				//var intent = new Intent(Intent.ActionView, url);
				//intent.AddFlags(ActivityFlags.NewTask);
				//Android.App.Application.Context.StartActivity(intent);
				//  Android.App.Application.Context.StartActivity(typeof(Activity1));
				PushPage();
			}
		}
		public Android.Views.View GetInfoContents(Marker marker)
		{
			var inflater = Android.App.Application.Context.GetSystemService(Android.Content.Context.LayoutInflaterService) as Android.Views.LayoutInflater;
			if (inflater != null)
			{
				Android.Views.View view;
			var customPin = marker;
				DropsView v = new DropsView(Context, null);
				v.Visibility = ViewStates.Visible;
				view = v;
				return view;
			}
			return null;
		}
		public Android.Views.View GetInfoWindow(Marker marker)
		{
			return null;
		}
		async public void PushPage()
		{
			// Do some Android specific things... and then push a new Forms' Page
			// await Xamarin.Forms.Application.Current.MainPage.Navigation.PushModalAsync(new MainPage());
			//await Xamarin.Forms.Application.Current.MainPage.Navigation.PushAsync(new Maplikwid031016.Map.MapView());
			//   App.showpage = true;
			await Xamarin.Forms.Application.Current.MainPage.Navigation.PushModalAsync(new MapView());
		}
		public bool OnClusterItemClick(Java.Lang.Object p0)
		{
			// var defaultme = new DefaultClusterRender(Context, map, _clusterManager);
			//// Toast.MakeText(Context,  "Marker clicked", ToastLength.Short).Show();
			DropsView v = new DropsView(Context, null);
			v.Visibility = ViewStates.Visible;
			Dialog viewdialog = new Dialog(Context, Resource.Style.dialogstyle);
			viewdialog.Window.SetGravity(GravityFlags.Top);
			viewdialog.Window.SetBackgroundDrawable(new ColorDrawable(Android.Graphics.Color.Transparent));
			var x = v.IsShown;
			var w = App.ScreenWidh;
			var h = App.ScreenHeigh;
			viewdialog.AddContentView(v, new LayoutParams(480, 1300));
			viewdialog.Show();
			return true;
		}
		public bool OnClusterClick(ICluster p0)
		{
			Toast.MakeText(Context, p0.Items.Count + " items in cluster", ToastLength.Short).Show();
			return true;
		}
		public void OnProviderDisabled(string provider)
		{
			Log.Debug(TAG, "OnProviderDisabled");
		}
		public void OnProviderEnabled(string provider)
		{
			Log.Debug(TAG, "OnProviderEnabled");
		}
		public void OnStatusChanged(string provider, [GeneratedEnum] Availability status, Bundle extras)
		{
			Log.Debug(TAG, "OnStatusChanged");
			_locationManager?.RemoveUpdates(this);
		}
		public void OnClusterInfoWindowClick(ICluster p0)
		{
			throw new NotImplementedException();
		}
		public void OnClusterItemInfoWindowClick(Java.Lang.Object p0)
		{
			throw new NotImplementedException();
		}
	}
}