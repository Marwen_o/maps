﻿using System;
using Android.Gms.Maps.Model;
using Com.Google.Maps.Android.Clustering;

namespace MapLikwid121016.Droid
{
	public class ClusterItem : Java.Lang.Object ,IClusterItem
	{
		public ClusterItem()
		{
		}
		public LatLng Position { get; set; }
		public CustomPin CustomPin { get; set; }
		public string Id { get; set; }
		public ClusterItem(double lat, double lng/*, MarkerOptions Markerpin*/)
		{
			this.Position = new LatLng(lat, lng);
		}

	}
}
