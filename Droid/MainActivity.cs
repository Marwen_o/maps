﻿using System;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

namespace MapLikwid121016.Droid
{
	[Activity(Label = "MapLikwid121016.Droid", Icon = "@drawable/icon", Theme = "@style/MyTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
	public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
	{
		protected override void OnCreate(Bundle bundle)
		{
			TabLayoutResource = Resource.Layout.Tabbar;
			ToolbarResource = Resource.Layout.Toolbar;

			base.OnCreate(bundle);

			global::Xamarin.Forms.Forms.Init(this, bundle);
			App.ScreenHeigh = (int)((float)Resources.DisplayMetrics.HeightPixels / (float)Resources.DisplayMetrics.Density);
			App.ScreenWidh = (int)((float)Resources.DisplayMetrics.WidthPixels / (float)Resources.DisplayMetrics.Density);
			LoadApplication(new App());
		}
	}
}
